# TencentDistrictSearch

#### 介绍
腾讯地图iOS SDK的行政区划检索功能Demo

#### 使用注意
由于代码体积问题，本Demo并没有集成腾讯地图SDK，需要自行[下载](https://lbs.qq.com/mobile/iOSMapSDK/mapDownload/download3D);

#### 使用说明
本Demo根据腾讯地图SDK提供的行政区划检索功能取得中国全部的一二三级行政区划，并通过PickerView展示。Demo只做了简单数据展示，有需要的开发者需要自行处理row的选择逻辑。