//
//  CityModel.m
//  TencentDistrictSearch
//
//  Created by v_hefang on 2020/9/14.
//  Copyright © 2020 v_hefang. All rights reserved.
//

#import "CityModel.h"

@implementation CityModel

+ (instancetype)cityWithDistrictData:(QMSDistrictData *)data {
    CityModel *model = [[CityModel alloc] init];
    model.cityId = data.id_;
    model.name = data.name;
    model.fullname = data.fullname;
    model.location = data.location;
    
    return model;
}


@end
