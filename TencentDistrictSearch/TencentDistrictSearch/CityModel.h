//
//  CityModel.h
//  TencentDistrictSearch
//
//  Created by v_hefang on 2020/9/14.
//  Copyright © 2020 v_hefang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "DistrictModel.h"
#import <QMapKit/QMSSearchKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityModel : NSObject

@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, assign) CLLocationCoordinate2D location;
@property (nonatomic, strong) NSArray<DistrictModel *> *districts;

+ (instancetype)cityWithDistrictData:(QMSDistrictData *)data;

@end

NS_ASSUME_NONNULL_END
