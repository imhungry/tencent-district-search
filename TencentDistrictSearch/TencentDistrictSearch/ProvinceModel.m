//
//  ProvinceModel.m
//  TencentDistrictSearch
//
//  Created by v_hefang on 2020/9/14.
//  Copyright © 2020 v_hefang. All rights reserved.
//

#import "ProvinceModel.h"

@implementation ProvinceModel

+ (instancetype)provinceWithDistrictData:(QMSDistrictData *)data {
    ProvinceModel *model = [[ProvinceModel alloc] init];
    model.provinceId = data.id_;
    model.name = data.name;
    model.fullname = data.fullname;
    model.location = data.location;
    
    return model;
}

@end
