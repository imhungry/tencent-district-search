//
//  AppDelegate.h
//  TencentDistrictSearch
//
//  Created by v_hefang on 2020/9/8.
//  Copyright © 2020 v_hefang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

