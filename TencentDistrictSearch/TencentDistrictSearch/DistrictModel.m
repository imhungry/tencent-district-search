//
//  DistrictModel.m
//  TencentDistrictSearch
//
//  Created by v_hefang on 2020/9/14.
//  Copyright © 2020 v_hefang. All rights reserved.
//

#import "DistrictModel.h"

@implementation DistrictModel

+ (instancetype)districtWithDistrictData:(QMSDistrictData *)data {
    DistrictModel *model = [[DistrictModel alloc] init];
    model.districtId = data.id_;
    model.name = data.name;
    model.fullname = data.fullname;
    model.location = data.location;
    
    return model;
}

@end
